# Impemented algorithms
* DMFW : Decentralized Meta Frank-Wolfe (Our algorithm)
* DROFW  : Decentralized Regularized Online Frank-Wolfe
* MFW : Meta Frank-Wolfe
* ROFW : Regularized Online Frank-Wolfe
* OFW : Online Frank-Wolfe
* FW : Frank-Wolfe 
* OAW : Online Away-step Frank-Wolfe
* MORGFW : Online stochastic Recursive Gradient-based Frank-Wolfe