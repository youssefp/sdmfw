function [solution, obj_values] = DMFW(G, X1, Xtrain, ytrain, num_iters, batch_size, eta_coef, eta_exp, reg_coef, loss_fn, gap_fn, grad_fn, print_freq, optimal_solution, L, oracle_type, radius)

% initialization
overhead_time = 0.0;

Xs = cell(0); % X(t,i,l) in the Algorithm
A = full(adjacency(G,'weighted'));
num_node = G.numnodes;
reg_coeff = reg_coef/sqrt(num_iters);


obj_values = zeros(fix(num_iters / print_freq) + 1, 5);


observed_X_cell_arr = cell(num_iters, num_node);
% filter of the data that each node can view on iteration t
observed_y_cell_arr = cell(num_iters, num_node);

t_start = tic;  % timing
% initializing oracles
oracles = cell(0); % init oracles for each node
for n = 1 : num_node
    for l = 1 : L
        oracle{l} = Oracle(size(X1), radius, oracle_type);
    end
    oracles{n} = oracle;
end

%X_old = cell(0); % X^t
%X_new = cell(0); % X^t+1
%X = cell(0);
for n = 1 : num_node
    for l = 1 : L+1
        X{l} = X1; % copy on write
    end
    Xs{n} = X;
end

for n = 1 : num_node
    %X_old{n} = X1;
    X_new{n} = X1;
end

for t = 1 : num_iters
    for n = 1 : num_node
        Xs{n}{1} = X1;
    end
    
    for l = 1 : L
        eta = min(eta_coef / (l + 1)^eta_exp, 1.0);
        % line 6 in the algorithm for complete graph
        Y = [];
        for m = 1 : num_node
            Y(:,:,m) = Xs{m}{l} + eta * (oracles{m}{l}.V - Xs{m}{l});
        end
        % line 7
        for n = 1 : num_node
            Xs{n}{l+1} = sum(Y .* reshape(A(:,n),1,1,num_node),3);
        end
    end
    
    for n = 1: num_node
        X_new{n} = Xs{n}{L+1}; % x^t+1 = x^t,i,L+1
    end
    
    % sample index
    idx = [(t-1) * (batch_size) + 1: t * (batch_size)];
    %store data, spliting the data accordingly to each node
    % (with equal splits)
    
    nodes = node_init(num_node,Xtrain(:,idx),ytrain(idx));
    for n = 1 : num_node
        observed_X_cell_arr{t,n} = nodes{1,n};
        observed_y_cell_arr{t,n} = nodes{2,n};
    end
    % the indexed t,num_node+1 is the entire batch without spliting
    observed_X_cell_arr{t,num_node+1} = Xtrain(:,idx);
    observed_y_cell_arr{t,num_node+1} = ytrain(idx);
    %evaluate loss function
    if mod(t, print_freq) == 0
        t_current = toc(t_start);
        running_time = t_current - overhead_time;
        curr_losss = [];
        curr_gaps = [];
        curr_grads = [];
        for n = 1 : num_node
            curr_losss(n) = loss_fn(X_new{n}, observed_X_cell_arr{t,num_node+1}, observed_y_cell_arr{t,num_node+1}) - loss_fn(optimal_solution, observed_X_cell_arr{t,num_node+1}, observed_y_cell_arr{t,num_node+1});
            curr_gaps(n) = gap_fn(X_new{n}, observed_X_cell_arr{t,num_node+1}, observed_y_cell_arr{t,num_node+1});
            % initialize for each node g^t,i,1
            curr_grads(:,:,n) = grad_fn(Xs{n}{1}, observed_X_cell_arr{t,n}, observed_y_cell_arr{t,n});
        end
        
        for l = 1 : L
            curr_ds = [];
            for n = 1 : num_node
                curr_ds(:,:,n) =  sum(curr_grads.*reshape(A(n,:),1,1,num_node),3);
                oracles{n}{l} = oracles{n}{l}.update(curr_ds(:,:,n).*reg_coeff);
            end
            curr_grads = [];
            for n = 1 : num_node
                curr_grads(:,:,n) = grad_fn(Xs{n}{l+1}, observed_X_cell_arr{t,n}, observed_y_cell_arr{t,n});
                curr_grads(:,:,n) = curr_grads(:,:,n) - grad_fn(Xs{n}{l}, observed_X_cell_arr{t,n}, observed_y_cell_arr{t,n});
                curr_grads(:,:,n) = curr_grads(:,:,n) + curr_ds(:,:,n);
            end
        end
        
        %regret value
        curr_loss = max(curr_losss);
        curr_gap = max(curr_gaps);
        obj_values(fix(t / print_freq) + 1, :) = [t, (t+1)*t/2 * batch_size, running_time, curr_loss, curr_gap];
        overhead_time = overhead_time + toc(t_start) - t_current;
    end
    
end
solution = X_new{1};
end
