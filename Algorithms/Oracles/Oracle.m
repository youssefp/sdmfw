classdef Oracle
    properties
        oracle_type; 
        oracle_name;
        G;
        V;
        t;
        r;
        options;
        
    end
    methods
        function obj = Oracle(dim,radius,t)
            if t == 'FTPL' 
                obj.oracle_type = t;
                obj.oracle_name = 'Follow The Perturbed Leader';
                obj.G = zeros(dim);
                obj.r = radius;    
                obj.V = lmo_fn(obj);
                obj.t = 1;
                %rng(50)
            elseif t == 'PGD'
                obj.oracle_type = t ;
                obj.oracle_name = 'Projected G-D';
                obj.V = zeros(dim);
                obj.feedbacks = cell(0);
                obj.t = 1;
                obj.r = radius;
            end
        end
        function res = lmo_fn(obj)  % lmo for l_1 constraint || W_j ||_1 <= radius
            [num_rows, num_cols] = size(obj.G);
            P = obj.G + (-0.5 + rand(size(obj.G,1),size(obj.G,2)));
            [~, rows] = max(abs(P), [], 1);
            cols = 1:num_cols;
            nz_values = - obj.r * sign(P(rows + (cols - 1) * num_rows));
            res = sparse(rows, cols, nz_values, num_rows, num_cols, num_cols);
        end
        function  obj = update_FTPL(obj,fb)
            obj.G = obj.G + fb;
            obj.V = lmo_fn(obj);
        end
        
        function res = proj_matrix_polytope(obj)
            res = obj;
            while (sum(sum(res.V<0) + (sum(res.V) > res.r)) > 0)
                res.V = max(0,res.V);
                s_card = sum(res.V > 0);
                if(sum(s_card) > 0)
                    d = ((sum(res.V) > 1).*(sum(res.V) - 1))./ s_card;
                    res.V = res.V - d .* (res.V > 0);
                end
            end
        end
        function  obj = update_PGD(obj,fb)
            obj.feedbacks{1}= fb;
            obj.t = obj.t + 1 ;
            obj.V = obj.V - 0.1 * fb;
            % basic projection (just to test the implementation)
            S = sign(obj.V);
            obj.V = abs(obj.V);
            obj = obj.proj_matrix_polytope();
            obj.V = obj.V .* S;
        end
        
        function obj = update(obj,fb)
            if obj.oracle_type == 'FTPL'
                obj = update_FTPL(obj,fb);
            elseif obj.oracle_type == 'PGD'
                obj = update_PGD(obj,fb);
            end
        end
    end
end

