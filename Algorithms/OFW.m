function [solution, obj_values] = OFW(W1, Xtrain, ytrain, num_iters, batch_size, eta_coef, eta_exp, loss_fn, grad_fn, lmo_fn, gap_fn, print_freq, optimal_solution)
    % initialization
    overhead_time = 0.0;
    W = W1;  % copy on write

    obj_values = zeros(fix(num_iters / print_freq) + 1, 5);
    % each row records [#iters, #SFO, running_time, loss_value, FW_gap];
    obj_values(1, :) = [0, 0, 0.0, 0.0, 0.0];
 
    observed_X_cell_arr = cell(num_iters, 1);
    observed_y_cell_arr = cell(num_iters, 1);

    t_start = tic;  % timing
    % rng(1);

    for t = 1 : num_iters
        eta = min(eta_coef / (t + 1)^eta_exp, 1.0);
        % sample an index
        
        idx = [(t-1) * batch_size + 1: t * batch_size];
       
        % store data
        observed_X_cell_arr{t} = Xtrain(:, idx);
        observed_y_cell_arr{t} = ytrain(idx);
        % update grad_estimate
        grad_estimate = zeros(size(W1));
        for tmp_t = 1 : t
            grad_estimate = grad_estimate + grad_fn(W, observed_X_cell_arr{tmp_t}, observed_y_cell_arr{tmp_t});
        end
        grad_esti_mate = grad_estimate ./ t;
        % LMO
        V = lmo_fn(grad_estimate);
        % update W
        W_old = W;
        W = (1 - eta) * W + eta * V;

        % evaluate loss function value and FW gap
        if mod(t, print_freq) == 0
            t_current = toc(t_start);
            running_time = t_current - overhead_time;
            
            curr_loss = loss_fn(W_old, observed_X_cell_arr{t}, observed_y_cell_arr{t}) - loss_fn(optimal_solution, observed_X_cell_arr{t}, observed_y_cell_arr{t});
            curr_gap = gap_fn(W_old, observed_X_cell_arr{t}, observed_y_cell_arr{t});
            obj_values(fix(t / print_freq) + 1, :) = [t, (t+1)*t/2 * batch_size, running_time, curr_loss, curr_gap];
           
            overhead_time = overhead_time + toc(t_start) - t_current;
        end
    end
    solution = W;
end
