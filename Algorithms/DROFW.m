function [solution, obj_values] = DROFW(G, X1, Xtrain, ytrain, num_iters, batch_size, eta_coef, eta_exp, reg_coef, lmo_fn, loss_fn, gap_fn, grad_fn, print_freq, optimal_solution)

% initialization
overhead_time = 0.0;

Xs = cell(0); % X(t,i,l) in the Algorithm
A = full(adjacency(G,'weighted'));
num_node = G.numnodes;
%reg_coeff = reg_coef/sqrt(num_iters);
reg_coef = reg_coef / (num_iters^(3/4) * num_node^(1/2));

obj_values = zeros(fix(num_iters / print_freq) + 1, 5);


% filter of the data that each node can view on iteration t
observed_X_cell_arr = cell(num_iters, num_node);
observed_y_cell_arr = cell(num_iters, num_node);

t_start = tic;  % timing
% rng(1);
% initializing oracles

Xs_old = cell(0); % X^t
Xs_new = cell(0); % X^t+1
Zs = cell(0);
for n = 1 : num_node
    Xs_old{n} = X1;
    Xs_new{n} = X1;
    Zs{n} = zeros(size(X1));
end

for t = 1 : num_iters
    eta = min(eta_coef / (t + 1)^eta_exp, 1.0);
    for n = 1 : num_node
        Xs_old{n} = Xs_new{n};
    end
    %% Batchs splits
    % sample index
    idx = [(t-1) * (batch_size) + 1: t * (batch_size)];
    %store data, spliting the data accordingly to each node
    % (with equal splits)
    nodes = node_init(num_node,Xtrain(:,idx),ytrain(idx));
    for n = 1 : num_node
        observed_X_cell_arr{t,n} = nodes{1,n};
        observed_y_cell_arr{t,n} = nodes{2,n};
    end
    % the indexed t,num_node+1 is the entire batch without spliting
    observed_X_cell_arr{t,num_node+1} = Xtrain(:,idx);
    observed_y_cell_arr{t,num_node+1} = ytrain(idx);
    
    %% Grad updates
    for n = 1 : num_node
        % initialize for each node g^t,i,1
        curr_grads(:,:,n) = grad_fn(Xs_old{n}, ...
            observed_X_cell_arr{t,n}, ...
            observed_y_cell_arr{t,n});
    end 
     for n = 1 : num_node
        tmp  = sum(Zs{n} .* reshape(A(:,n),1,1,num_node),3);
        Zs{n} = tmp + curr_grads(:,:,n);
    end
     for n = 1 : num_node
        curr_Fs{n} =  Zs{n} + Xs_old{n} * 2 / reg_coef ;
        curr_Vs{n} = lmo_fn(curr_Fs{n});
        Xs_new{n} = (1-eta) * Xs_old{n} + eta * curr_Vs{n};
        
    end
    %evaluate loss function
    if mod(t, print_freq) == 0
        t_current = toc(t_start);
        running_time = t_current - overhead_time;
        for n = 1 : num_node
            curr_losss(n) = loss_fn(Xs_old{n}, ...
                observed_X_cell_arr{t,num_node+1},...
                observed_y_cell_arr{t,num_node+1}) ...
                - loss_fn(optimal_solution,...
                observed_X_cell_arr{t,num_node+1},...
                observed_y_cell_arr{t,num_node+1});
            curr_gaps(n) = gap_fn(Xs_old{n},...
                observed_X_cell_arr{t,num_node+1},...
                observed_y_cell_arr{t,num_node+1});
        end
        %regret value
        curr_loss = max(curr_losss);
        curr_gap = max(curr_gaps);
        obj_values(fix(t / print_freq) + 1, :) = [t, (t+1)*t/2 * batch_size, running_time, curr_loss, curr_gap];
        overhead_time = overhead_time + toc(t_start) - t_current;
        
    end
    solution = Xs_new{1};
end
