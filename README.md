### DATASETS
1. Download the MNIST and CIFAR-10 datasets. Please see `Data/MNIST/README.md` and `Data/CIFAR-10/README.md` for details.

2. Launch `MATLAB` from the `utils` directory, and the run `LoadMNIST.m` and `LoadCifar10.m` to generate the dataset files.

### HOW TO RUN
1. For the multi-class logistic regression experiment, first open `main.m` and choose one configuration script file, for example

``` matlab

%% Section 2: Set parameters (choose one configuration script and comment the others, the configuration scripts can be found in the Config folder)
if ~exist('IS_TUNING_PARAMETERS', 'var') || IS_TUNING_PARAMETERS == false
    % MNIST_adversary;
    % CIFAR10_adversary;
end
```
You can change the topology of the graph by modifying the `graph_config.m` file in the Config folder.
There are predefined graph construction functions in `Utils\Graph` folder.
In Oracles folder, it is also possible to add a new type of Oracle in `Oracle.m`. 
We implemented the Follow-the-Perturbed-Leader (FTPL) and a Gradient-descent type oracles. 

After saving changes to `main.m` and  `graph_config.m`, to execute the experiment type `main` in the MATLAB command window.

### PROJECT STRUCTURE 

```bash
├── Algorithms
│   ├── DMFW_array.m 
│   ├── DMFW.m ----- (Distributed Meta Frank Wolfe === Our distributed online algorithm with exact gradients)
│   ├── DROFW_array.m
│   ├── DROFW.m ----- (Distributed Regularized Online Frank Wolfe)
│   ├── FW.m ----- (Frank Wolfe)
│   ├── MFW_array.m
│   ├── MFW.m ----- (Centralized Meta Frank Wolfe)
│   ├── MORGFW.m
│   ├── OAW.m
│   ├── OFW.m ----- (Centralized Online Frank Wolfe)
│   ├── Oracles
│   │   └── Oracle.m ----- (Follow The Perturbed Leader [This oracle is a class object you can modify to your preffered online oracle])
│   ├── README.md
│   ├── ROFW.m ----- (Centralized Regularized Online Frank Wolfe)
│   ├── SDMFW_array.m
│   └── SDMFW.m ----- (Stochastic Distributed Meta FranK Wolfe === Our distributed online algorithm with stochastic gradient estimates)
├── Config ----- (Configure the hyperparemeters and the network for distributed algorithms)
│   ├── CIFAR10_adversary.m
│   ├── graph_config.m
│   ├── MNIST_adversary.m
│   └── README.md
├── Data
│   ├── CIFAR-10
│   │   └── README.md
│   ├── CIFAR10_LR_opt.mat
│   ├── MNIST
│   │   └── README.md
│   └── MNIST_LR_opt.mat
├── main.m
├── README.md
└── Utils ----- (Utility functions)
    ├── DataHandler ----- (To load the data)
    │   ├── LoadCifar10.m
    │   ├── LoadMNIST.m
    │   └── node_init.m
    └── Graphs ----- (To generate network topologies)
        ├── completebintree.m
        ├── completegraph.m
        ├── compute_weights.m
        ├── cyclegraph.m
        ├── gridcrossgraph.m
        ├── gridgraph.m
        ├── randbintree.m
        ├── torusgraph.m
        └── weight_my_graph.m
```
