
dataset = 'CIFAR10';  % Xtrain, ytrain, Xtest, ytest, classes, optimal_value
model_radius = 32.0;

selected_methods = { ...
    'SDMFW',...% our stochastic distributed online algorithm
    'DMFW',... % our non stochastic distributed online algorithm
    'DROFW'... 
    %'MFW',...
    %'MORGFW',...
    %'OFW',...
    %'ROFW'...
    %'OAW'...
    };

batch_size = 5e2;
sub_batch_size = 4;
% batch_size = 1;  % batch_size=1 works like a charm
print_freq = 1e0;  % the frequency of progress evaluation, e.g., evaluate the loss function after every 100 iterations
num_iters_base = 1e2;  % number of iterations of the baseline method (OSFW)

eta_coef_ORGFW = 1.25e-0;
eta_exp_ORGFW = 1;
rho_coef_ORGFW = 0.75e-0;
rho_exp_ORGFW = 2/3;

eta_coef_OSFW = 1e-0;
eta_exp_OSFW = 1;
rho_coef_OSFW = 1e-0;
rho_exp_OSFW = 2/3;

eta_coef_FW = 2.5e-1;
eta_exp_FW = 1.0;

eta_coef_OFW = 5e-2;
eta_exp_OFW = 1;

eta_coef_ROFW = 5e-2;
eta_exp_ROFW = 1;
reg_coef_ROFW = 1e2;
num_time_ROFW = 1;

eta_coef_OAW = 5e-2;
eta_exp_OAW = 1;

eta_coef_MFW = 0.1e-0;
eta_exp_MFW = 1;
rho_coef_MFW = 1e-0;
rho_exp_MFW = 1/2;
reg_coef_MFW = 1e2;
num_time_MFW=1;


eta_coef_DMFW = 0.1e-0;
eta_exp_DMFW = 1;
rho_coef_DMFW = 1e-0;
rho_exp_DMFW = 1/2;
reg_coef_DMFW = 1e2;
L_DMFW = 50;
num_time_DMFW = 1;
oracle_type = 'FTPL';
oracle_name = Oracle(0,0,oracle_type).oracle_name;


eta_coef_SDMFW = 0.1e-0;
eta_exp_SDMFW = 1;
rho_coef_SDMFW = 1e-0;
rho_exp_SDMFW = 1/2;
reg_coef_SDMFW = 1e2;
L_SDMFW = 50;
num_time_SDMFW = 1;

eta_coef_MORGFW = 1e-1;
eta_exp_MORGFW = 1;
rho_coef_MORGFW = 5e-1;
rho_exp_MORGFW = 1/2;
reg_coef_MORGFW = 1e2;
