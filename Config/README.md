## Configuration files 

### `CIFAR10_config.m` and `MNIST_config.m`
In these config files, you can choose the hyperparameters for each algorithm and decide which algorithms to run for the corresponding dataset (*MNIST* and *CIFAR10*)

### `graph_config.m` 
In this config file, you can choose a stochasticaly weighted graph and it's nam for the distributed algorithms. 
And you can also change the online minimization oracles for the algortihm DMFW. (by default it is set to Follow The Perturbed Leader)