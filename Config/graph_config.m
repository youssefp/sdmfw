
%% Oracle config
oracle_type = 'FTPL';
oracle_name = Oracle(0,0,oracle_type).oracle_name;

%% Topology config
% You can use all the predefined function that generates graphs with their
% name in /Utilts/Graph 
% completegraph(n) : K_n
% cyclegraph(n) : C_n 
% gridgraph(n,m) : Grid_n,m
% gridcrossgraph(n,m) : XGrid_n,m (This is a grid graph with diagonals 
% completebintree(h) : T_h (Complete binary tree with height h) 
% torusgraph(n,m) : Torus_n,m (Grid graph of size n * m without edges)

[G,graph_name] = gridgraph(5,10);

%% If you want to use your own persenal graph G to experiment,
%  Use the function weight_my_graph on G to compute the proper weights. 
%  Example : 
%    G = my_graph();
%    G = weight_my_graph(G); 
