%% Section 1: Include necessary files
addpath('./Algorithms');
addpath('./Algorithms/Oracles');
addpath('./Config');
addpath('./Utils/DataHandler');
addpath('./Utils/Graphs');
DATA_ROOT = './Data/';
import Oracle.*;

%% Section 2.1: Set parameters (choose one configuration script 
% and comment the others, 
% the configuration scripts can be found in the Config folder)
if ~exist('IS_TUNING_PARAMETERS', 'var') || IS_TUNING_PARAMETERS == false
    MNIST_adversary;
    %CIFAR10_adversary;
end
%% Section 2.2: Configuration of the Oracle and the topology 
%  for the distributed algorithm
%  Change Config/graph_config file to change the topology
graph_config; 
%% Section 3: Load data
data_file_name = [DATA_ROOT, dataset, '_dataset.mat'];
load(data_file_name, 'Xtrain', 'ytrain', 'Xtest', 'ytest');
[dimension, data_size] = size(Xtrain);
num_classes = length(unique(ytrain));

optimal_value_file_name = [DATA_ROOT, dataset, '_LR_opt.mat'];
if exist(optimal_value_file_name, 'file') == 2
    load(optimal_value_file_name, 'optimal_value', 'optimal_solution');
    IS_OPTIMAL_VALUE_AVAILABLE = true;
else
    IS_OPTIMAL_VALUE_AVAILABLE = false;
end


%% Section 4: Run algorithms, do NOT modify the code below
num_iters_ORGFW = num_iters_base;
num_iters_OSFW = num_iters_base;
num_iters_OFW = num_iters_base;
print_freq_OFW = print_freq;
num_iters_ROFW = num_iters_base;
num_iters_OAW = num_iters_base;
print_freq_OAW = print_freq;
num_iters_MFW = num_iters_base;
num_iters_DMFW = num_iters_base;
num_iters_MORGFW = num_iters_base;
num_iters_FW = ceil(num_iters_base * batch_size * 2 / data_size);

W0 = zeros(dimension, num_classes);
loss_handle = @loss_fn;
grad_handle = @grad_fn;
lmo_handle = @(V)lmo_fn(V, model_radius);
gap_handle = @(W, X, y)gap_fn(W, X, y, model_radius);
SDMFW_handle = @SDMFW_array;
DMFW_handle = @DMFW_array;
MFW_handle = @MFW_array;
DROFW_handle = @DROFW_array;

obj_values_cell = cell(length(selected_methods), 1);

for method_idx = 1 : length(selected_methods)
    curr_method = selected_methods{method_idx};
    if strcmp(curr_method, 'SDMFW') == true % Distributed MFW
        curr_label = [curr_method + " " + graph_name +  ', \eta coef=' +  num2str(eta_coef_DMFW) + ', \eta exp=' + num2str(eta_exp_DMFW) + '\rho coef=' + num2str(rho_coef_DMFW)];
        fprintf('%s\n',curr_label);
        args = {G, W0, Xtrain, ytrain, num_iters_DMFW, batch_size, eta_coef_SDMFW, eta_exp_SDMFW, reg_coef_SDMFW, loss_handle, gap_handle, grad_handle, print_freq, optimal_solution, L_SDMFW, oracle_type, model_radius};
        [solution, obj_values] = run_algorithm(SDMFW_handle,args,num_time_SDMFW);
    elseif strcmp(curr_method, 'DMFW') == true % Distributed ROFW
        curr_label = [curr_method + " " + graph_name +  ', \eta coef=' +  num2str(eta_coef_DMFW) + ', \eta exp=' + num2str(eta_exp_DMFW) + '\rho coef=' + num2str(rho_coef_DMFW)];
        fprintf('%s\n',curr_label);
        args = {G, W0, Xtrain, ytrain, num_iters_DMFW, batch_size, eta_coef_DMFW, eta_exp_DMFW, reg_coef_DMFW, loss_handle, gap_handle, grad_handle, print_freq, optimal_solution, L_DMFW, oracle_type, model_radius};
        [solution, obj_values] = run_algorithm(DMFW_handle,args,num_time_DMFW);  
    elseif strcmp(curr_method, 'DROFW') == true % Distributed ROFW
        cur_method = curr_method + graph_name;
        curr_label = [curr_method + " " + graph_name + '\eta coef=' + num2str(eta_coef_ROFW) + ', \eta exp=' + num2str(eta_exp_ROFW) + num2str(reg_coef_ROFW)];
        fprintf('%s\n',curr_method);
        args = {G, W0, Xtrain, ytrain, num_iters_DMFW, batch_size, eta_coef_ROFW, eta_exp_ROFW, reg_coef_ROFW, lmo_handle, loss_handle, gap_handle, grad_handle, print_freq, optimal_solution};
        [solution, obj_values] = run_algorithm(DROFW_handle,args,num_time_ROFW);
    elseif strcmp(curr_method, 'FW') == true  % Full FW
        curr_label = [curr_method, ', \eta coef=', num2str(eta_coef_FW), ', \eta exp=', num2str(eta_exp_FW)];
        fprintf('%s\n', curr_label);
        [solution, obj_values] = FW(W0, Xtrain, ytrain, num_iters_FW, eta_coef_FW, eta_exp_FW, loss_handle, grad_handle, lmo_handle, gap_handle);
    elseif strcmp(curr_method, 'OFW') == true  % OFW
        curr_label = [curr_method, ', \eta coef=', num2str(eta_coef_OFW), ', \eta exp=', num2str(eta_exp_OFW)];
        fprintf('%s\n', curr_label);
        [solution, obj_values] = OFW(W0, Xtrain, ytrain, num_iters_OFW, batch_size, eta_coef_OFW, eta_exp_OFW, loss_handle, grad_handle, lmo_handle, gap_handle, print_freq_OAW, optimal_solution);
    elseif strcmp(curr_method, 'ROFW') == true  % ROFW
        curr_label = [curr_method, ', \eta coef=', num2str(eta_coef_ROFW), ', \eta exp=', num2str(eta_exp_ROFW), ', reg coef=', num2str(reg_coef_ROFW)];
        fprintf('%s\n', curr_label);
        [solution, obj_values] = ROFW(W0, Xtrain, ytrain, num_iters_ROFW, batch_size, eta_coef_ROFW, eta_exp_ROFW, reg_coef_ROFW, loss_handle, grad_handle, lmo_handle, gap_handle, print_freq, optimal_solution);
    elseif strcmp(curr_method, 'OAW') == true  % OAW
        curr_label = [curr_method, ', \eta coef=', num2str(eta_coef_OAW), ', \eta exp=', num2str(eta_exp_OAW)];
        fprintf('%s\n', curr_label);
        [solution, obj_values] = OAW(W0, Xtrain, ytrain, num_iters_OAW, batch_size, eta_coef_OAW, eta_exp_OAW, loss_handle, grad_handle, lmo_handle, gap_handle, print_freq_OAW, optimal_solution);
    elseif strcmp(curr_method, 'MFW') == true  % MFW
        curr_label = [curr_method, ', \eta coef=', num2str(eta_coef_MFW), ', \eta exp=', num2str(eta_exp_MFW), ', \rho coef=', num2str(rho_coef_MFW), ', \rho exp=', num2str(rho_exp_MFW)];
        fprintf('%s\n', curr_label);
        args = {W0, Xtrain, ytrain, num_iters_MFW, batch_size, sub_batch_size, eta_coef_MFW, eta_exp_MFW, rho_coef_MFW, rho_exp_MFW, reg_coef_MFW, loss_handle, grad_handle, lmo_handle, gap_handle, print_freq, optimal_solution};
        [solution, obj_values] = run_algorithm(MFW_handle,args,num_time_MFW);
    elseif strcmp(curr_method, 'MORGFW') == true  % MORGFW
        curr_label = [curr_method, ', \eta coef=', num2str(eta_coef_MORGFW), ', \eta exp=', num2str(eta_exp_MORGFW), ', \rho coef=', num2str(rho_coef_MORGFW), ', \rho exp=', num2str(rho_exp_MORGFW), ', reg coef=', num2str(reg_coef_MORGFW)];
        fprintf('%s\n', curr_label);
        [solution, obj_values] = MORGFW(W0, Xtrain, ytrain, num_iters_MORGFW, batch_size, sub_batch_size, eta_coef_MORGFW, eta_exp_MORGFW, rho_coef_MORGFW, rho_exp_MORGFW, reg_coef_MORGFW, loss_handle, grad_handle, lmo_handle, gap_handle, print_freq, optimal_solution);
    else
        error('method name must be ROFW, OSFW, OFW, OAW, MORGFW, MFW, FW or DOMFW');
    end
    
    % plot curves
    if IS_OPTIMAL_VALUE_AVAILABLE 
            plot(obj_values(:, 1), cumsum(obj_values(:, 4)), 'DisplayName', curr_label, 'LineWidth', 1.5); hold on;
            xlabel('#iterations');
            ylabel('regret');
            legend('show', 'Location', 'northwest');
    else
            plot(obj_values(:, 1), obj_values(:, 4), 'DisplayName', curr_label); hold on;
            xlabel('#iterations');
            ylabel('loss value');
            legend('show');
    end
    obj_values_cell{method_idx} = obj_values;

    % training accuracy

    training_accuracy = evaluate_accuracy_fn(solution, Xtrain, ytrain);
    fprintf('training accuracy: %f\n', training_accuracy);

end

% save the experimental results
output_file_name = [DATA_ROOT, 'results_', dataset, '_l1_auto_save.mat'];
save(output_file_name, 'selected_methods', 'obj_values_cell');

grid on;


%% Section 5: Definitions of loss function, gradient, 
%  and linear optimization oracle

% objective function: multiclass logistic regression, 
% see section 5 of https://arxiv.org/pdf/1902.06332
% f(W) = - \sum_{i=1}^N \sum_{c=1}^C 1{y_i = c} log( (exp(w_c^T x_i)) /
% (\sum_{j=1}^C exp(w_j^T x_i))), s.t. ||W||_1 <= 1

function [solution,obj_values] = run_algorithm(algo, args, num_times)
    [mean_solution, mean_obj_values] = algo(args);
    for i = 2 : num_times
        [tmp_solution, tmp_obj_values] = algo(args);
        mean_solution = mean_solution + tmp_solution;
        mean_obj_values = mean_obj_values + tmp_obj_values;
    end
    solution = mean_solution ./ num_times;
    obj_values = mean_obj_values ./ num_times;
end

function loss = loss_fn(W, X, y)
    data_size = size(X, 2);
    tmp_exp = exp(W' * X);
    tmp_numerator = zeros(1, data_size);
    for i = 1 : data_size
        tmp_numerator(i) = tmp_exp(y(i), i);
    end
    loss = - mean(log(tmp_numerator ./ sum(tmp_exp, 1))); % average loss
end


function grad = grad_fn(W, X, y)
    data_size = size(X, 2);
    tmp_exp = exp(W' * X);
    tmp_denominator = sum(tmp_exp, 1);
    tmp_exp = tmp_exp ./ tmp_denominator;

    for i = 1 : data_size
        tmp_exp(y(i), i) = tmp_exp(y(i), i) - 1;
    end
    grad = (X./data_size) * tmp_exp';
end

function res = lmo_fn(V, radius)  % lmo for l_1 constraint || W_j ||_1 <= radius
    [num_rows, num_cols] = size(V);
    [~, rows] = max(abs(V), [], 1);
    cols = 1:num_cols;
    nz_values = - radius * sign(V(rows + (cols - 1) * num_rows));
    res = sparse(rows, cols, nz_values, num_rows, num_cols, num_cols);
end

function fw_gap = gap_fn(W, X, y, radius)
    grad = grad_fn(W, X, y);
    fw_gap = sum(sum(grad .* (W - lmo_fn(grad, radius))));
end

function accuracy = evaluate_accuracy_fn(W, X, y)
    data_size = size(X, 2);
    scores = W' * X;
    [~, predict_classes] = max(scores);
    accuracy = sum(y' == predict_classes) / data_size;
end
