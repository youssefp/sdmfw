function nodes = node_init(num_node, Xtrain, ytrain)
nodes = cell([4, num_node]);
XtrainT = transpose(Xtrain);
ytraintmp = ytrain;
for i = 0:(num_node-2)
    cvtrain = cvpartition(size(XtrainT,1),'Holdout',1/(num_node-i));
    idxtrain = cvtrain.test;
    nodes{1,i+1} = transpose(XtrainT(idxtrain,:));
    nodes{2,i+1} = ytraintmp(idxtrain,:);
    XtrainT = XtrainT(~idxtrain,:);
    ytraintmp = ytraintmp(~idxtrain,:);
    
end
nodes{1,num_node} = transpose(XtrainT);
nodes{2,num_node} = ytraintmp;
end


