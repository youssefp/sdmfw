function G = weight_my_graph(H)
    A = compute_weights(H);
    G = graph(A);
end

