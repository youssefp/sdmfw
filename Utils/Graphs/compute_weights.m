function WA = compute_weights(G)
    [Ei,Ej] = findedge(G);
    WA =  zeros(G.numnodes);
    for e = 1 : G.numedges
        if Ei(e) ~= Ej(e)
            WA(Ei(e),Ej(e)) = 1 / (1 + max(degree(G,Ei(e)),degree(G,Ej(e))));
        end
    end
    WA = WA + transpose(WA);
    for i = 1 : G.numnodes 
        WA(i,i) = 1 -  sum(WA(i,G.neighbors(i)));
    end
    
end

