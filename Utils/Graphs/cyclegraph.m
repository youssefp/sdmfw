function [G,graph_name] = cyclegraph(n)
    A = zeros(n);
    for i = 1 : n-1
        A(i,i+1) = 1; 
    end
    A(1,n) = 1; 
    A = A + transpose(A);
    G = graph(A);
    A = compute_weights(G);
    G = graph(A);
    graph_name = "C_{"+num2str(n)+"}";
end

