function [G,graph_name] = gridgraph(n,m)
    G = graph;
    for i = 1: n
        for j = 1 : m
            if i < n 
                G = addedge(G, string(i)+","+string(j),string(i+1)+","+string(j));
            end
            if i > 1 
                G = addedge(G, string(i)+","+string(j),string(i-1)+","+string(j));
            end
            if j < m
                G = addedge(G, string(i)+","+string(j),string(i)+","+string(j+1));
            end
            if j > 1 
                G = addedge(G, string(i)+","+string(j),string(i)+","+string(j-1));
            end
        end
    end
    
    G = simplify(G);
    A = compute_weights(G);
    G = graph(A);
    graph_name = "Grid_{"+num2str(n)+"\times"+num2str(m)+"}";
end
