function [T,graph_name] = completebintree(height)
    T = graph;
    if height == 0  
        T = graph(1);
    end
    for l = 0 : height-1
        for i = 0 : 2^l-1 
            T = addedge(T,2^l+i,2^(l+1)+2*i); 
            T = addedge(T,2^l+i,2^(l+1)+2*i+1);
        end
    end
    T = simplify(T); 
    A = compute_weights(T);
    T = graph(A);
    graph_name = 'Complete T_{'+num2str(height)+'}';       
end