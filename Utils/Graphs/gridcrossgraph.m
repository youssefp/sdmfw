function [G,graph_name] = gridcrossgraph(n,m)
    G = graph;
    for i = 1: n
        for j = 1 : m
            if i < n
                G = addedge(G, string(i)+","+string(j),string(i+1)+","+string(j));
            end
            if i >1 
                G = addedge(G, string(i)+","+string(j),string(i-1)+","+string(j));
            end
            if j < m
                G = addedge(G, string(i)+","+string(j),string(i)+","+string(j+1));
            end
            if j > 1 
                G = addedge(G, string(i)+","+string(j),string(i)+","+string(j-1));
            end
            if i < n && j < m
                G = addedge(G,string(i)+","+string(j),string(i+1)+","+string(j+1));
            end
            if i < n && j > 1
                G = addedge(G,string(i)+","+string(j),string(i+1)+","+string(j-1));
            end
            if i > 1 && j < m
                G = addedge(G,string(i)+","+string(j),string(i-1)+","+string(j+1));
            end
            if i > 1 && j>1
                G = addedge(G,string(i)+","+string(j),string(i-1)+","+string(j-1));
            end
            
        end
    end
  
    G = simplify(G);
    A = compute_weights(G);
    G = graph(A);
    graph_name = "XGrid_{"+num2str(n)+","+num2str(m)+"}";

end
