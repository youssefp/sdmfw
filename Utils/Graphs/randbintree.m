function [T,graph_name] = randbintree(height,p)
    T = graph;
    if height == 0  
        T = graph(1);
    end
    for l = 0 : height-1
        for i = 0 : 2^l-1 
            if rand() < p 
                T = addedge(T,2^l+i,2^(l+1)+2*i); 
            else 
                T = addedge(T,2^l, 2^(l+1)+2*i);
            end
            if rand() < p 
                T = addedge(T,2^l+i,2^(l+1)+2*i+1);
            else 
                T = addedge(T,2^l, 2^(l+1)+2*i+1);
            end
        end
    end
    T = simplify(T); 
    A = compute_weights(T);
    T = graph(A);
    graph_name = "Rand BinTree w/ Height = " + num2str(height)+ ", p = " + num2str(p);
end