function [G,graph_name] = completegraph(n)
    A = compute_weights(graph(ones(n)));
    G = graph(A);
    graph_name = "K_{"+num2str(n)+"}";
end

